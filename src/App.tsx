import React from "react";
import "./App.css";
import { TodoList } from "./components/TodoList";
import { TodoItem } from "./types";
import { TodoListAddItem } from "./components/TodoListAddItem";
import { useLocalStorage } from "./hooks/useLocalStorage";

const App = () => {
  const [todos, setTodos] = useLocalStorage<TodoItem[]>("data", []);

  const removeItem = (index: number) => {
    const filteredTodos = todos.filter(_ => _.index !== index);
    setTodos(filteredTodos);
  };

  const checkItem = (index: number) => {
    setTodos(
      todos.map(_ => {
        if (_.index === index) _.checked = !_.checked;
        return _;
      })
    );
  };

  const addItem = (text: string) => {
    let maxIndex =
      todos.length > 0
        ? Math.max.apply(
            null,
            todos.map(_ => _.index)
          )
        : 0;
    const newTodo = {
      index: maxIndex + 1,
      text,
      checked: false
    };

    setTodos([...todos, newTodo]);
  };

  const changeText = (index: number, text: string) => {
    setTodos(
      todos.map(_ => {
        if (_.index === index) _.text = text;
        return _;
      })
    );
  };

  return (
    <div className="App">
      <header className="App-header">
        <TodoList
          todos={todos}
          removeItem={removeItem}
          checkItem={checkItem}
          changeText={changeText}
          addItemElement={TodoListAddItem({ addItem: addItem })}
        />
      </header>
    </div>
  );
};

export default App;
