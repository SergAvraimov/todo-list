export interface TodoItem {
    index: number,
    text: string,
    checked: boolean
}