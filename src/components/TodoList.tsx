import React from "react";
import { TodoItem } from "../types";
import { TodoListItem } from "./TodoListItem";
import { createUseStyles } from "react-jss";

interface TodoListProps {
  todos: TodoItem[];
  addItemElement: any;
  checkItem: (index: number) => void;
  removeItem: (index: number) => void;
  changeText: (index: number, text: string) => void;
}

export function TodoList(props: TodoListProps) {
  const classes = useStyles();

  const todosSorted = props.todos.sort((a, b) => {
    if (a.checked > b.checked) {
      return 1;
    }
    if (a.checked < b.checked) {
      return -1;
    }

    if (a.index > b.index) {
      return -1;
    }
    if (a.index < b.index) {
      return 1;
    }

    return 0;
  });

  const todoItems = todosSorted.map(item => (
    <li key={item.index} className={classes.todoListItem}>
      <TodoListItem
        item={item}
        removeItem={props.removeItem}
        checkItem={props.checkItem}
        changeText={props.changeText}
      />
    </li>
  ));

  return (
    <ul className={classes.todoList}>
      <li className={classes.todoListItem}>{props.addItemElement}</li>
      {todoItems}
    </ul>
  );
}

const useStyles = createUseStyles({
  todoList: {
    listStyleType: "none",
    backgroundColor: "#ffffff",
    width: "90%",
    maxWidth: 800,
    paddingLeft: 0,
    alignSelf: "center",
    border: "1px solid #d0d0d0"
  },
  todoListItem: {
    boxShadow: "0 -16px 0 -15px #dfdfdf",
    height: "40px",

    "&:hover": {
      background: "#f6f6f6"
    }
  }
});
